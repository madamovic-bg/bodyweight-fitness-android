package io.mazur.fit.utils;

import android.util.Log;

public class Logger {
    public static void e(String msg) { Log.e("Bodyweight Fitness", msg); }
    public static void d(String msg) { Log.d("Bodyweight Fitness", msg); }
}
